# kover-serialization-issue1

## Description

Kover, using Kotlin 1.9.10, shows a different result vs Kotlin 1.8.10 when run on a serializable data class that
provides a simple list property. This project provides two modules allowing you to build and test the data class
generating coverage data using Kotlin 1.8.10 and 1.9.10.

The expectation is that both modules should show the same coverage report. The reality is that they do not.

The report generated for Kotlin 1.9.10 does not provide any insight into what must be done to get to 100% coverage.

To see this errant behavior, run "gradlew clean build" in each of the two modules.
