package com.pajato.kover.bug2331

import kotlinx.serialization.Serializable

@Serializable
data class Common(val data: List<String>)
