package com.pajato.kover.bug2331

import kotlin.test.Test
import kotlin.test.assertEquals

class CommonUnitTest {

    @Test fun `When creating a default common instance, verify the empty list`() {
        val common: Common = Common(listOf())
        assertEquals(0, common.data.size)
    }

    @Test fun `When creating a non-default common instance, verify the data field`() {
        val common = Common(listOf("xyzzy", "abcd"))
        assertEquals(2, common.data.size)
    }
}