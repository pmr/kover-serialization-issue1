import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.8.10"
    kotlin("plugin.serialization") version "1.8.10"
    id("org.jetbrains.kotlinx.kover") version "0.7.3"
}

group = "com.pajato"
version = "1.0-SNAPSHOT"

koverReport {
    defaults {
        html {
            onCheck = true
            setReportDir(layout.buildDirectory.dir("coverage-report"))
        }

        verify {
            onCheck = true
            rule {
                isEnabled = true
                entity = kotlinx.kover.gradle.plugin.dsl.GroupingEntityType.APPLICATION

                bound {
                    minValue = 100
                    metric = kotlinx.kover.gradle.plugin.dsl.MetricType.INSTRUCTION
                    aggregation = kotlinx.kover.gradle.plugin.dsl.AggregationType.COVERED_PERCENTAGE
                }
            }
        }
    }
    filters { excludes { classes(listOf("**.*serializer*")) } }
}

repositories {
    mavenCentral()
}

dependencies {
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.5.1")
    testImplementation(kotlin("test"))
}

tasks.withType<KotlinCompile> { kotlinOptions.jvmTarget = "17" }

java { toolchain.languageVersion.set(JavaLanguageVersion.of(17)) }
